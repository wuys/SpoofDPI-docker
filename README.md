# SpoofDPI - Dockerized version

Just a dockerized version of [SpoofDPI](https://github.com/xvzc/SpoofDPI)

## Usage

Move `.env.example` to `.env`, then change the default configuration:

```
ADDR=0.0.0.0
DEBUG=false
DNS_ADDR=1.1.1.1
#DNS_PORT=53
ENABLE_DOH=true
#PATTERN=google|github
PORT=8080
SYSTEM_PROXY=true
TIMEOUT=0
#WINDOW_SIZE=10
```

Start SpoofDPI with `docker compose` and enjoy

```shell
docker compose up -d
```

## Credits

- [SpoofDPI](https://github.com/xvzc/SpoofDPI)
