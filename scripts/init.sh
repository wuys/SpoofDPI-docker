#!/bin/sh

REPO="xvzc/SpoofDPI"
REPO_API="https://api.github.com/repos/$REPO/releases/latest"
REPO_TAG="https://github.com/$REPO/releases/download"

# Required packages & remove cache
apk -qU upgrade && apk add -q curl tar
rm -rf /var/cache/apk/*

# Get latest version
LATEST_TAG=$(curl -s $REPO_API | grep '"tag_name":' | sed -E 's/.*"([^"]+)".*/\1/')
LATEST_BIN="$REPO_TAG/$LATEST_TAG/spoofdpi-linux-amd64.tar.gz"
curl -sLo spoof-dpi-linux.tar.gz "$LATEST_BIN"

# Install SpoofDPI
tar -xzvf spoof-dpi-linux.tar.gz
rm -f spoof-dpi-linux.tar.gz
