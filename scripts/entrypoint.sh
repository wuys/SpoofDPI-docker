#!/bin/sh

# Base command
CMD="/app/spoofdpi"

# Initialize arguments list
ARGS=""

# Conditionally append optional arguments
[ -n "${ADDR}" ] && ARGS="$ARGS -addr ${ADDR}"
[ "${DEBUG}" = "true" ] && ARGS="$ARGS -debug"
[ -n "${DNS_ADDR}" ] && ARGS="$ARGS -dns-addr ${DNS_ADDR}"
[ -n "${DNS_PORT}" ] && ARGS="$ARGS -dns-port ${DNS_PORT}"
[ "${ENABLE_DOH}" = "true" ] && ARGS="$ARGS -enable-doh"
[ -n "${PATTERN}" ] && ARGS="$ARGS -pattern ${PATTERN}"
[ -n "${PORT}" ] && ARGS="$ARGS -port ${PORT}"
[ "${SYSTEM_PROXY}" = "true" ] && ARGS="$ARGS -system-proxy"
[ -n "${TIMEOUT}" ] && ARGS="$ARGS -timeout ${TIMEOUT}"
[ -n "${WINDOW_SIZE}" ] && ARGS="$ARGS -window-size ${WINDOW_SIZE}"

# Execute the command with all arguments
exec $CMD $ARGS
