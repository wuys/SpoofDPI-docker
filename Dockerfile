FROM alpine:latest AS base
FROM base AS download

WORKDIR /app

COPY scripts/init.sh /init.sh

RUN chmod +x /init.sh && /init.sh && rm /init.sh

FROM base AS runner

WORKDIR /app

COPY --from=download /app/spoofdpi /app

COPY scripts/entrypoint.sh /app

RUN chmod +x /app/spoofdpi /app/entrypoint.sh

EXPOSE ${PORT}

ENTRYPOINT ["/app/entrypoint.sh"]
